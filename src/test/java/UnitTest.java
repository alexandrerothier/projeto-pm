import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;


public class UnitTest {
    private Context ctx = mock(Context.class);

    @Test
    public void GET_busca_funcionario() {
        when(ctx.queryParam("nome")).thenReturn("Alice");
        Controller.encontrarFuncionario(ctx); // the handler we're testing
        verify(ctx).status(200);
    }

//    @Test(expected = BadRequestResponse.class)
//    public void POST_to_create_users_throws_for_invalid_username() {
//        when(ctx.queryParam("username")).thenReturn(null);
//        UserController.create(ctx); // the handler we're testing
//    }
}

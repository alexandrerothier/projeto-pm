package Aposentados;

import java.util.UUID;

public class Aposentado {
    UUID idFuncionario;
    UUID idObjeto;
    String tipo;

    public Aposentado(UUID idFuncionario, UUID idObjeto, String tipo) {
        this.idFuncionario = idFuncionario;
        this.idObjeto = idObjeto;
        this.tipo = tipo;
    }

    public UUID getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(UUID idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    public UUID getIdObjeto() {
        return idObjeto;
    }

    public void setIdObjeto(UUID idObjeto) {
        this.idObjeto = idObjeto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}

package Aposentados;

import Consertos.Conserto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AposentadoRepository {
    private List<Aposentado> aposentados;
    private static AposentadoRepository instance;

    private AposentadoRepository() {
        this.aposentados = new ArrayList<>();

    }

    public static AposentadoRepository getInstance() {
        if(instance == null) {
            instance = new AposentadoRepository();
        }
        return instance;
    }

    public void criarAposentadoriaBicicleta(UUID funcionario, UUID bicicleta) {
        for (Aposentado aposentado: aposentados) {
            if(funcionario.equals(aposentado.getIdFuncionario()) && bicicleta.equals(aposentado.getIdObjeto())){
                return;
            }
        }
        aposentados.add(new Aposentado(funcionario, bicicleta, "Bicicleta"));
    }

    public void criarAposentadoriaTotem(UUID funcionario, UUID totem) {
        for (Aposentado aposentado: aposentados) {
            if(funcionario.equals(aposentado.getIdFuncionario()) && totem.equals(aposentado.getIdObjeto())){
                return;
            }
        }
        aposentados.add(new Aposentado(funcionario, totem, "Totem"));
    }

    public void criarAposentadoriaTranca(UUID funcionario, UUID tranca) {
        for (Aposentado aposentado: aposentados) {
            if(funcionario.equals(aposentado.getIdFuncionario()) && tranca.equals(aposentado.getIdObjeto())){
                return;
            }
        }
        aposentados.add(new Aposentado(funcionario, tranca, "Tranca"));
    }
}
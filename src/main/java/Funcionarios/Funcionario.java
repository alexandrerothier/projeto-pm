package Funcionarios;

import com.google.gson.JsonObject;

import java.util.UUID;

public class Funcionario {
	UUID id;
	String nome;
	int matricula;
	boolean indicadorBr;
	String tipo;
	int numero;

	public static Funcionario JsonParaFuncionario(JsonObject json) {
			String id = json.get("id").getAsString();
			String nome = json.get("nome").getAsString();
			int matricula = json.get("matricula").getAsInt();
			boolean indicadorBr = json.get("indicadorBr").getAsBoolean();
			JsonObject jsonDocumento =  json.get("documento").getAsJsonObject();
			String tipo = jsonDocumento.get("tipo").getAsString();
			int numero = jsonDocumento.get("numero").getAsInt();

			Funcionario funcionario = new Funcionario(UUID.fromString(id), nome, matricula, indicadorBr, tipo, numero);

			return funcionario;
	}


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public boolean isIndicadorBr() {
		return indicadorBr;
	}

	public void setIndicadorBr(boolean indicadorBr) {
		this.indicadorBr = indicadorBr;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Funcionario(UUID id, String nome, int matricula, boolean indicadorBr, String tipo, int numero) {
		this.id = id;
		this.nome = nome;
		this.matricula = matricula;
		this.indicadorBr = indicadorBr;
		this.tipo = tipo;
		this.numero = numero;
	}
}

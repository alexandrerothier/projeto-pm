package Funcionarios;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FuncionarioRepository {
        private List<Funcionario> funcionarios;
        private static FuncionarioRepository instance;

    private FuncionarioRepository() {
        this.funcionarios = new ArrayList<>();
        this.funcionarios.add(new Funcionario(UUID.fromString("7db88970-09ab-4607-9605-8914b69ee7c7"), "Alice", 1111, true, null, 0));
        }


    public void criarFuncionario(Funcionario funcionario){
        funcionarios.add(funcionario);
    }

    public void deletarFuncionario(Funcionario funcionario){
        funcionarios.remove(funcionario);
    }

        public Funcionario encontrarFuncionarioId(UUID id){
        for (Funcionario funcionario : funcionarios){
                if(funcionario.getId().equals(id)){
                    return funcionario;
                }
            }
            return null;
        }

        public List<Funcionario> encontrarFuncionarioNome(String nome){
        List<Funcionario> found = new ArrayList<>();
        for (Funcionario funcionario : funcionarios){
                if(funcionario.getNome().contains(nome)){
                    found.add(funcionario);
                }
            }
            return found;
        }

    public List<Funcionario> encontrarFuncionarioMatricula(int matricula){
        List<Funcionario> found = new ArrayList<>();
        for (Funcionario funcionario : funcionarios){
            if(funcionario.getMatricula() == matricula){
                found.add(funcionario);
            }
        }
        return found;
    }

    public static FuncionarioRepository getInstance(){
        if(instance == null)
            instance = new FuncionarioRepository();
        return instance;
    }

}

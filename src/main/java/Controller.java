import Funcionarios.Funcionario;
import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.*;

import java.util.UUID;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

// This is a controller, it should contain logic related to client/server IO
public class Controller {

    @OpenApi(summary = "Encontra funcionario", operationId = "encontrarFuncionario", path = "/funcionario", method = HttpMethod.GET, pathParams = {
            @OpenApiParam(name = "id", type = String.class, description = "ID do Funcionario"),
            @OpenApiParam(name = "nome", type = String.class, description = "Nome do Funcionario"),
            @OpenApiParam(name = "matricula", type = Integer.class, description = "Matricula do Funcionario"),
            @OpenApiParam(name = "indicadorBr", type = Boolean.class, description = "Brasileiro"),
            @OpenApiParam(name = "tipo", type = String.class, description = "Tipo do Documento"),
            @OpenApiParam(name = "numero", type = Integer.class, description = "Número do Documento")},
            tags = {"Funcionario"},
            responses = {
            @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Funcionario.class)}),
            @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
            @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})}
            )

    public static void encontrarFuncionario(Context ctx) {
        Services s = new Services();
        UUID id;
        String nome;
        int matricula;

        try {
            id = UUID.fromString(ctx.queryParam("id"));
        } catch (Exception e) {
            id = null;
        }

        try{
            nome = ctx.queryParam("nome");
        }catch (Exception e) {
            nome = null;
        }

        try{
            matricula = Integer.valueOf(ctx.queryParam("matricula"));
        }catch (Exception e) {
            matricula = 0;
        }

        try {
            ctx.json(s.retornarFuncionario(id, nome, matricula));
            ctx.status(200);
        } catch (Exception e) {
            ctx.status(400);
        }
    }

    public static void criarFuncionario(Context ctx) {
        Services s = new Services();

        try {
            JsonObject json = new JsonParser().parse(ctx.body()).getAsJsonObject();
            Funcionario funcionario = Funcionario.JsonParaFuncionario(json);
            s.criarFuncionario(funcionario);
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }

    }

    public static void deletarFuncionario(Context ctx) {
        Services s = new Services();
        UUID id = UUID.fromString(ctx.queryParam("id"));
        s.deletarFuncionario(id);
    }

    public static void consertaBicicleta(Context ctx) {
        Services s = new Services();

        try {
            s.consertarBicicleta(ctx.queryParam("Funcionario"), (ctx.queryParam("Bicicleta")));
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void aposentaBicicleta(Context ctx) {
        Services s = new Services();

        try {
            s.aposentaBicicleta(ctx.queryParam("Funcionario"), (ctx.queryParam("Bicicleta")));
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void consertaTotem(Context ctx) {
        Services s = new Services();

        try {
            s.consertarTotem(ctx.queryParam("Funcionario"), (ctx.queryParam("Totem")));
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void aposentaTotem(Context ctx) {
        Services s = new Services();

        try {
            s.aposentaTotem(ctx.queryParam("Funcionario"), (ctx.queryParam("Totem")));
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void consertaTranca(Context ctx) {
        Services s = new Services();

        try {
            s.consertarTranca(ctx.queryParam("Funcionario"), (ctx.queryParam("Tranca")));
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void aposentaTranca(Context ctx) {
        Services s = new Services();

        try {
            s.aposentaTranca(ctx.queryParam("Funcionario"), (ctx.queryParam("Tranca")));
            ctx.status(201);
        } catch (Exception e){
            ctx.status(400);
        }
    }
}

    
import Aposentados.AposentadoRepository;
import Consertos.ConsertoRepository;
import Funcionarios.Funcionario;
import Funcionarios.FuncionarioRepository;

import java.util.*;

// This is a service, it should be independent from Javalin
public class Services {

    public List<Funcionario> retornarFuncionario(UUID id, String nome, int matricula) {
        List<Funcionario> funcionarios = new ArrayList<>();
        if (id != null) {
            Funcionario funcionario = FuncionarioRepository.getInstance().encontrarFuncionarioId(id);
            if (funcionario != null)
                funcionarios.add(funcionario);
        } else if (matricula != 0) {
            funcionarios = FuncionarioRepository.getInstance().encontrarFuncionarioMatricula(matricula);
        } else {
            funcionarios = FuncionarioRepository.getInstance().encontrarFuncionarioNome(nome);
        }
        return funcionarios;
    }

    public void criarFuncionario(Funcionario funcionario) {
        FuncionarioRepository.getInstance().criarFuncionario(funcionario);
    }

    public void deletarFuncionario(UUID id) {
        if (FuncionarioRepository.getInstance().encontrarFuncionarioId(id) != null) {
            Funcionario funcionario = FuncionarioRepository.getInstance().encontrarFuncionarioId(id);
            FuncionarioRepository.getInstance().deletarFuncionario(funcionario);
        }
    }

    public void consertarBicicleta(String funcionario, String bicicleta) {
        ConsertoRepository.getInstance().criarConsertoBicicleta(UUID.fromString(funcionario), UUID.fromString(bicicleta));
    }

    public void consertarTotem(String funcionario, String totem) {
        ConsertoRepository.getInstance().criarConsertoTotem(UUID.fromString(funcionario), UUID.fromString(totem));
    }

    public void consertarTranca(String funcionario, String tranca) {
        ConsertoRepository.getInstance().criarConsertoTranca(UUID.fromString(funcionario), UUID.fromString(tranca));
    }

    public void aposentaBicicleta(String funcionario, String bicicleta) {
        AposentadoRepository.getInstance().criarAposentadoriaBicicleta(UUID.fromString(funcionario), UUID.fromString(bicicleta));
    }

    public void aposentaTotem(String funcionario, String totem) {
        AposentadoRepository.getInstance().criarAposentadoriaTotem(UUID.fromString(funcionario), UUID.fromString(totem));
    }

    public void aposentaTranca(String funcionario, String tranca) {
        AposentadoRepository.getInstance().criarAposentadoriaTranca(UUID.fromString(funcionario), UUID.fromString(tranca));

    }
}

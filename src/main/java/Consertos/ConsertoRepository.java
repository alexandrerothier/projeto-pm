package Consertos;

import Funcionarios.Funcionario;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ConsertoRepository {
    private List<Conserto> consertos;
    private static ConsertoRepository instance;

    private ConsertoRepository() {
        this.consertos = new ArrayList<Conserto>();

    }

    public static ConsertoRepository getInstance(){
        if(instance == null)
            instance = new ConsertoRepository();
        return instance;
    }

    public void criarConsertoBicicleta(UUID funcionario, UUID bicicleta) {
        for (Conserto conserto: consertos) {
          if(funcionario.equals(conserto.idFuncionario) && bicicleta.equals(conserto.getIdObjeto())){
              return;
          }
        }
        consertos.add(new Conserto(funcionario, bicicleta, "bicicleta"));
    }

    public void criarConsertoTotem(UUID funcionario, UUID totem) {
        for (Conserto conserto: consertos) {
            if(funcionario.equals(conserto.idFuncionario) && totem.equals(conserto.getIdObjeto())){
                return;
            }
        }
        consertos.add(new Conserto(funcionario, totem, "totem"));
    }

    public void criarConsertoTranca(UUID funcionario, UUID tranca) {
        for (Conserto conserto: consertos) {
            if(funcionario.equals(conserto.idFuncionario) && tranca.equals(conserto.getIdObjeto())){
                return;
            }
        }
        consertos.add(new Conserto(funcionario, tranca, "tranca"));
    }
}
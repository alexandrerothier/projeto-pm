package Objetos;

import java.util.UUID;

public class Totem {
    UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}

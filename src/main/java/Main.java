import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import io.javalin.Javalin;
import io.javalin.http.Handler;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.models.Path;

public class Main {
	public static void main(String[] args) {
		Javalin app = Javalin.create(config -> {
			config.registerPlugin(getConfiguredOpenApiPlugin());
			config.defaultContentType = "application/json";
		}).start(getHerokuAssignedPort());
		app.routes(() -> {
			path("funcionario", () -> {
				get(Controller::encontrarFuncionario);
				post(Controller::criarFuncionario);
				delete(Controller::deletarFuncionario);
			});
			path("bicicleta", () -> {
				path("/consertar", () -> {
					post(Controller::consertaBicicleta);
				});
				path("/aposentar", () -> {
					post(Controller::aposentaBicicleta);
				});
			});
			path("totem", () -> {
				path("/consertar", () -> {
					post(Controller::consertaTotem);
				});
				path("/aposentar", () -> {
					post(Controller::aposentaTotem);
				});
			});
			path("tranca", () -> {
				path("/consertar", () -> {
					post(Controller::consertaTranca);
				});
				path("/aposentar", () -> {
					post(Controller::aposentaTranca);
				});
			});
		});

		System.out.println("Check out ReDoc docs at http://localhost:8080/redoc");
		System.out.println("Check out Swagger UI docs at http://localhost:8080/swagger-ui");
	}

	private static OpenApiPlugin getConfiguredOpenApiPlugin() {
		Info info = new Info().version("").title("").description("");
		OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("")
				.path("/swagger-docs") // endpoint for OpenAPI json
				.swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
				.reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
				.defaultDocumentation(doc -> {
					doc.json("500", ErrorResponse.class);
					doc.json("503", ErrorResponse.class);

				});
		return new OpenApiPlugin(options);
	}

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 8080;
	}
}
